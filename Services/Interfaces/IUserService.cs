﻿using SelectorReact.Model;

namespace SelectorReact.Services.Interfaces
{
    public interface IUserService
    {
        User GetUserData(int? userId);
        void AddOrUpdateUserData(SaveRequestModel saveRequestModel, Microsoft.AspNetCore.Http.ISession session);
    }
}
