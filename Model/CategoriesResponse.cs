﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SelectorReact.Model
{
    public class CategoriesResponse
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("categories")]
        public List<Sector> Sectors { get; set; }

        [JsonProperty("sectors")]
        public List<UserSector> UserSector { get; set; }
    }
}
