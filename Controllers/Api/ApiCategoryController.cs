﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SelectorReact.Data;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;

namespace SelectorReact.Controllers.Api
{
    [Route("api/categories")]
    [ApiController]
    public class ApiCategoryController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ISectorService _sectorService;
        private readonly ICategoryService _categoryService;

        public ApiCategoryController(HelmesSelectorContext context, IUserService userService,
            ISectorService sectorService, ICategoryService categoryService)
        {
            _userService = userService;
            _sectorService = sectorService;
            _categoryService = categoryService;
            context.Database.EnsureCreated();
        }

        // GET: Categories
        public async Task<JsonResult> Index()
        {
            var categorySectorsModel = _categoryService.GetCategoryModels();

            var userId = HttpContext.Session.GetString("UserId");
            User userModel;
            if (userId == null)
            {
                userModel = _userService.GetUserData(null);
            }
            else
            {
                userModel = _userService.GetUserData(Convert.ToInt32(userId));
            }

            List<UserSector> userSectorsModel = new List<UserSector>();

            if (userModel.Id != 0)
            {
                userSectorsModel = await _sectorService.GetAllSectors(userModel.Id);
            }

            return new JsonResult(JsonConvert.SerializeObject(new CategoriesResponse
            {
                User = userModel,
                Sectors = categorySectorsModel,
                UserSector = userSectorsModel
            }, 
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
        }
    }
}