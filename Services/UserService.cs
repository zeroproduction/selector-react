﻿using SelectorReact.Data;
using SelectorReact.Facade;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SelectorReact.Services
{
    public class UserService : IUserService
    {
        private readonly UserServiceHelper _userServiceHelper;

        public UserService(HelmesSelectorContext helmesSelectorContext)
        {
            _userServiceHelper = new UserServiceHelper(helmesSelectorContext);
        }

        public User GetUserData(int? userId)
        {
            return _userServiceHelper.GetUserData(userId);
        }

        public void AddOrUpdateUserData(SaveRequestModel saveRequestModel, Microsoft.AspNetCore.Http.ISession session)
        {
            var user = new User()
            {
                Name = saveRequestModel.Name,
                AgreementToTerms = saveRequestModel.AgreementToTerms,
                UserSectors = new List<UserSector>()
            };

            foreach (var sectorId in saveRequestModel.Sectors)
            {
                user.UserSectors.Add(new UserSector() { SectorId = sectorId });
            }

            Task.Run(async () => await _userServiceHelper.AddOrUpdateUserData(user, session)).Wait();
        }
    }
}
