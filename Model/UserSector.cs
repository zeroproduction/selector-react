﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SelectorReact.Model
{
    public class UserSector
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        public int SectorId { get; set; }
        public int UserId { get; set; }

        public Sector Sector { get; set; }
        public User User { get; set; }
    }
}

