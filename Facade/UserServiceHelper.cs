﻿using Microsoft.AspNetCore.Http;
using SelectorReact.Data;
using SelectorReact.Model;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SelectorReact.Facade
{
    public class UserServiceHelper
    {
        private readonly HelmesSelectorContext _helmesSelectorContext;

        public UserServiceHelper(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public User GetUserData(int? userId)
        {
            try
            {
                if (userId == null)
                    return new User();
                var userModel = _helmesSelectorContext.User.Find(userId);
                return userModel ?? new User();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new User();
            }
        }

        public async Task<bool> AddOrUpdateUserData(User user, ISession session)
        {
            try
            {
                var userId = session.GetString("UserId");
                User userData;
                if (userId != null)
                {
                    userData = GetUserData(int.Parse(userId));
                }
                else
                {
                    userData = GetUserData(null);
                }

                return (userData.Id == 0) ? await AddUserModel(user, session)
                                         : await UpdateUserModelAsync(user, userData);

            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return false;
            }
        }

        public async Task<bool> AddUserModel(User user, ISession session)
        {
            try
            {
                await _helmesSelectorContext.User.AddAsync(user);
                await _helmesSelectorContext.SaveChangesAsync();

                session.SetString("UserId", Convert.ToString(user.Id));
                return true;
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return false;
            }
        }

        public async Task<bool> UpdateUserModelAsync(User user, User old)
        {
            try
            {
                old.Name = user.Name;
                old.AgreementToTerms = user.AgreementToTerms;
                old.UserSectors = user.UserSectors;

                var userSectors = _helmesSelectorContext.UserSector.Where(x => x.UserId == old.Id).ToList();
                _helmesSelectorContext.UserSector.RemoveRange(userSectors);

                _helmesSelectorContext.User.Update(old);
                await _helmesSelectorContext.SaveChangesAsync();
                return true;
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return false;
            }
        }
    }
}
