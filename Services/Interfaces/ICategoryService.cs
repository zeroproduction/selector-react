﻿using SelectorReact.Model;
using System.Collections.Generic;

namespace SelectorReact.Services.Interfaces
{
    public interface ICategoryService
    {
        List<Sector> GetCategoryModels();
    }
}
