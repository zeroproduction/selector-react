﻿using Microsoft.EntityFrameworkCore;
using SelectorReact.Data;
using SelectorReact.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelectorReact.Facade
{
    public class SectorServiceHelper
    {

        private readonly HelmesSelectorContext _helmesSelectorContext;

        public SectorServiceHelper(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public async Task<List<UserSector>> GetUserSectorsData(int id)
        {
            try
            {
                return await _helmesSelectorContext.UserSector.Where(x => x.UserId == id).ToListAsync();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new List<UserSector>();
            }
        }
    }
}
