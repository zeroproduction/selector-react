﻿import React, { Component } from 'react';

export default class UserAddField extends Component {
    static displayName = UserAddField.name;

	constructor(props) {
		super(props);

		var { user } = props;

		this.onHandleInputFieldValueChanged = this.onHandleInputFieldValueChanged.bind(this);

		this.state = {
			user: user
		};
	}

    onHandleInputFieldValueChanged(event) {
        var user = this.state.user;
        user.name = event.target.value
        this.setState({user: user});
	    this.props.addNewUser({
            name: user.name
        });
    }

	render() {
        return (
			<div className="col-sm-10">
                <input type="text" className="form-control" defaultValue={this.state.user.name} onChange={this.onHandleInputFieldValueChanged} />
	        </div>
		);
	}
}
