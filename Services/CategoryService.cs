﻿using SelectorReact.Data;
using SelectorReact.Facade;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;
using System.Collections.Generic;

namespace SelectorReact.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly CategoryServiceHelper _categoryServiceHelper;

        public CategoryService(HelmesSelectorContext helmesSelectorContext)
        {
            _categoryServiceHelper = new CategoryServiceHelper(helmesSelectorContext);
        }

        public List<Sector> GetCategoryModels()
        {
            return _categoryServiceHelper.GetCategoryModels();
        }
    }
}
