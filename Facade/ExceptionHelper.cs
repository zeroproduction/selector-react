﻿using System;

namespace SelectorReact.Facade
{
    public class ExceptionHelper
    {
        public static void HandleException(Exception exception)
        {
            Console.WriteLine(exception.Message);
            Console.WriteLine(exception.StackTrace);
        }
    }
}
