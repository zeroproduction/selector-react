﻿using Newtonsoft.Json;

namespace SelectorReact.Model
{
    public class ApiResponse
    {
        [JsonProperty("has_error")]
        public bool HasError { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}
