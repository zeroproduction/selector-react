﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SelectorReact.Data;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;

namespace SelectorReact.Controllers.Api
{
    [Route("api/ApiForm")]
    [ApiController]
    public class ApiFormController : ControllerBase
    {
        private readonly IUserService _userService;

        public ApiFormController(HelmesSelectorContext context, IUserService userService)
        {
            _userService = userService;
            context.Database.EnsureCreated();
        }

        [HttpPost]
        public JsonResult Index([FromBody] SaveRequestModel saveRequestModel)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult(JsonConvert.SerializeObject(new ApiResponse
                {
                    HasError = true,
                    Content = "Not Valid Data"
                },
                Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }));
            }
            
            _userService.AddOrUpdateUserData(saveRequestModel, HttpContext.Session);

            return new JsonResult(JsonConvert.SerializeObject(new ApiResponse
            {
                HasError = false,
                Content = string.Empty
            },
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
        }
    }
}