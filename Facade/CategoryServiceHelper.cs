﻿using Microsoft.EntityFrameworkCore;
using SelectorReact.Data;
using SelectorReact.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SelectorReact.Facade
{
    public class CategoryServiceHelper
    {
        private readonly HelmesSelectorContext _helmesSelectorContext;

        public CategoryServiceHelper(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public List<Sector> GetCategoryModels()
        {
            try
            {
                return _helmesSelectorContext.Sector.Include(x => x.ChildSectors).OrderBy(x => x.Name)
                    .AsEnumerable().Where(x => x.ParentId == null).ToList();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new List<Sector>();
            }
        }
    }
}
